# GeneFlow variant calling pipeline test data and procedure

1. Download data from git
```
git clone https://gitlab.com/Niranjanpandeshwar/snp_geneflow
```

2. Change directory into snp_geneflow
```
cd snp_geneflow
```

3. Install required geneflow and singularity modules
```
ml geneflow/v2.0.0-alpha.4
ml singularity/2.5-patch
```

4. Install the variant calling pipeline workflow and corresponding apps. the format to be followed is "gf install-workflow <name> —make-apps -g <git_link>
```
gf install-workflow pipeline —make-apps -g https://git.biotech.cdc.gov/geneflow-workflows/variant-calling-vcftools-picard-gf2
```

5. Run the workflow by passing required and optional parameter. --in.reference for folder containing fasta file. Suppose the fasta file is ref1.fasta, then the corresponding folder name should be ref1. --in.files for folder containing input fastq files. --param.gatk_ploidy for ploidy level. (1 for haploid, 2 for diploid). pass your reference folder name as --param.refprefix parameter.
```
gf run ./ --in.reference ../reference_file/wildtype --in.files ../input/ --param.gatk_ploidy 2 --param.refprefix 'wildtype'
```
6. Go to your home directory to find the output files. cd into geneflow-output and find the your run as the latest entry or find the job name in the log after running the workflow. Then cd into that folder to find your output files. You can find the sample output files in this repository
```
cd geneflow-output
```